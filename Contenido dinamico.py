# -*- coding: utf-8 -*-
"""
El siguiente código es complemento de la notebook 
sobre el manejo de contenido estático de una paǵina 
de la clase de web scrapping.

La idea aquí es que en muchas páginas la información
se muestra en forma dinámica, por lo que vamos a necesitar
manejar cosas de javascript. Esto lo podemos hacer con 
selenium, que simula un navegador web como si fuera un 
usuario más
"""
# ---------- Importación de librerías ----------- #

# Importamos algunos módulos de selenium que nos van a servir
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# Importamos la libreŕía time para esperar cierto tiempo
import time 

# ---------------------------------------------- #

# ------------ Creación del navegador --------- #

# Para que selenium ande hay que descargar un driver, que depende de cada 
# navegador y sistema operativo, y se descarga en la página de selenium:
# https://selenium-python.readthedocs.io/installation.html#drivers

# Para especificar donde está se lo pasamos 
# como argumento cuando inicializamos el navegador. 
# Para linux, hay que convertirlo en ejecutable chmod +x driver
# Para mas detalles:
# https://stackoverflow.com/questions/42478591/python-selenium-chrome-webdriver
PATH_DRIVER = '/home/usr/folders/driver' # Cambiar aquí dónde está

# Creamos un navegador tipo Chrome. 
# Podemos decirle que navegue en forma explícita o ímplícita.
# La segunda lo logramos descomentando la línea "--headless".
chrome_options = Options()
#chrome_options.add_argument("--headless")

# Creación del navegador
driver = webdriver.Chrome(executable_path = PATH_DRIVER, options = chrome_options)

# -------------------------------------------- #

# --------- Visitamos la página -------------- #

# Visitamos la página y esperamos 5 segundos a que todo cargue bien
driver.get("https://www.lanacion.com.ar")
time.sleep(5)

# Acá manejamos algo dinámico:
# Dado que en La Nación el contenido aparece scrolleando vamos 
# a hacer eso. Todos las líneas de código en javascript se pueden
# ejecutar con ".execute_script" e insertando el código correspondiente allí.
# Lo que vamos a hacer acá es scrollear varias veces hasta el final 
# de la página y esperar un poco a que se cargue bien la página

for iteration in range(6):
    try:
        driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
        time.sleep(5)
    except:
        pass

# Una vez que dejamos scrollear identicamos todos los bloques asociados 
# a las notas (ver notebook)
# Cómo se busca en selenium? https://selenium-python.readthedocs.io/locating-elements.html
elements = driver.find_elements_by_class_name("com-title")

# Ahora guardamos los enlaces de cada elemento
hrefs = []
for element in elements:
    try:
	href = element.find_element_by_tag_name("a").get_attribute("href")
	hrefs.append(href)
    except:
        pass

# Guardamos los enlaces en un archivo
fp = open('links.txt','w')
for href in hrefs:
	fp.write(href + '\n')
fp.close()

# Finalmente cerramos todas las sesiones de navegación que abrimos 
# Son necesarias ambas líneas (una cierra la pestaña, la otra toda el navegador)
driver.close()
driver.quit()
