# -*- coding: utf-8 -*-

""" Este código utiliza scrapy para extraer sistemáticamente
un conjunto de páginas que están indexadas y por lo tanto
son fáciles de recorrer.
Independientemente de esto, se puede levantar enlaces guardados
en un archivo. 
La forma de funcionar es definiendo una clase donde le indicamos 
dónde está cada elemento de interés. 
Luego se corre desde la terminal como:

scrapy runspider ejemplo_scrapy.py -o data.json 

guardando los datos en formato json y por lo tanto más fácil
de trabajar 
"""

import scrapy

# Defino un objeto donde van a parar los datos
class NewspapersItem(scrapy.Item):

    title = scrapy.Field()
    body = scrapy.Field()
    date = scrapy.Field()
    section = scrapy.Field()
    url = scrapy.Field()

# Defino la araña que va a recorrer las páginas 
class Pagina12Spider(scrapy.Spider):
    name = "pagina12"

    # Aquí le decimos qué páginas tiene que scrappear 
    def start_requests(self):
	
	init_id = 345000
	final_id = 345068

        urls = []
        for i in range(init_id, final_id):
            urls.append('http://www.pagina12.com.ar/' + str(i))

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse, meta = {'dont_merge_cookies': True})

    # Este método tiene todas la información de dónde está cada cosa
    def parse(self, response):

        try:
            date = response.selector.xpath('//div[@class = "date"]//text()')[0].extract()
        except:
            date = ''

        try:
            title = response.selector.xpath('//h1/text()')[0].extract()
        except:
            title = ''

        try: 
            body = response.selector.xpath('//div[@class = "article-main-content article-text "]//text()').extract()
	    body = ' '.join(body)
        except: 
            body = ''

        try:
            section = response.selector.xpath('//*[@class = "current-tag"]//text()').extract()
            section = ' '.join(section)
        except:
            section = ''

	# Guardamos los datos en el objeto que creamos arriba
        item = NewspapersItem()
        item['title'] = title
        item['date'] = date
        item['section'] = section
	item['body'] = body
        item['url'] = response.url

        return item
