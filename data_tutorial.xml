<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
  <link href="http://arxiv.org/api/query?search_query%3Dall%3Anetwork%26id_list%3D%26start%3D0%26max_results%3D5" rel="self" type="application/atom+xml"/>
  <title type="html">ArXiv Query: search_query=all:network&amp;id_list=&amp;start=0&amp;max_results=5</title>
  <id>http://arxiv.org/api/tPE9XocbOXlz0ZowKf7LtUNnyFY</id>
  <updated>2021-05-25T00:00:00-04:00</updated>
  <opensearch:totalResults xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/">175521</opensearch:totalResults>
  <opensearch:startIndex xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/">0</opensearch:startIndex>
  <opensearch:itemsPerPage xmlns:opensearch="http://a9.com/-/spec/opensearch/1.1/">5</opensearch:itemsPerPage>
  <entry>
    <id>http://arxiv.org/abs/2105.11450v1</id>
    <updated>2021-05-24T17:58:36Z</updated>
    <published>2021-05-24T17:58:36Z</published>
    <title>SAT: 2D Semantics Assisted Training for 3D Visual Grounding</title>
    <summary>  3D visual grounding aims at grounding a natural language description about a
3D scene, usually represented in the form of 3D point clouds, to the targeted
object region. Point clouds are sparse, noisy, and contain limited semantic
information compared with 2D images. These inherent limitations make the 3D
visual grounding problem more challenging. In this study, we propose 2D
Semantics Assisted Training (SAT) that utilizes 2D image semantics in the
training stage to ease point-cloud-language joint representation learning and
assist 3D visual grounding. The main idea is to learn auxiliary alignments
between rich, clean 2D object representations and the corresponding objects or
mentioned entities in 3D scenes. SAT takes 2D object semantics, i.e., object
label, image feature, and 2D geometric feature, as the extra input in training
but does not require such inputs during inference. By effectively utilizing 2D
semantics in training, our approach boosts the accuracy on the Nr3D dataset
from 37.7% to 49.2%, which significantly surpasses the non-SAT baseline with
the identical network architecture and inference input. Our approach
outperforms the state of the art by large margins on multiple 3D visual
grounding datasets, i.e., +10.4% absolute accuracy on Nr3D, +9.9% on Sr3D, and
+5.6% on ScanRef.
</summary>
    <author>
      <name>Zhengyuan Yang</name>
    </author>
    <author>
      <name>Songyang Zhang</name>
    </author>
    <author>
      <name>Liwei Wang</name>
    </author>
    <author>
      <name>Jiebo Luo</name>
    </author>
    <link href="http://arxiv.org/abs/2105.11450v1" rel="alternate" type="text/html"/>
    <link title="pdf" href="http://arxiv.org/pdf/2105.11450v1" rel="related" type="application/pdf"/>
    <arxiv:primary_category xmlns:arxiv="http://arxiv.org/schemas/atom" term="cs.CV" scheme="http://arxiv.org/schemas/atom"/>
    <category term="cs.CV" scheme="http://arxiv.org/schemas/atom"/>
  </entry>
  <entry>
    <id>http://arxiv.org/abs/2105.11444v1</id>
    <updated>2021-05-24T17:54:27Z</updated>
    <published>2021-05-24T17:54:27Z</published>
    <title>MillimeterDL: Deep Learning Simulations of the Microwave Sky</title>
    <summary>  We present 500 high-resolution, full-sky millimeter-wave Deep Learning (DL)
simulations that include lensed CMB maps and correlated foreground components.
We find that these MillimeterDL simulations can reproduce a wide range of
non-Gaussian summary statistics matching the input training simulations, while
only being optimized to match the power spectra. The procedure we develop in
this work enables the capability to mass produce independent full-sky
realizations from a single expensive full-sky simulation, when ordinarily the
latter would not provide enough training data. We also circumvent a common
limitation of high-resolution DL simulations that they be confined to small sky
areas, often due to memory or GPU issues; we do this by developing a
"stitching" procedure that can faithfully recover the high-order statistics of
a full-sky map without discontinuities or repeated features. In addition, since
our network takes as input a full-sky lensing convergence map, it can in
principle take a full-sky lensing convergence map from any large-scale
structure (LSS) simulation and generate the corresponding lensed CMB and
correlated foreground components at millimeter wavelengths; this is especially
useful in the current era of combining results from both CMB and LSS surveys,
which require a common set of simulations.
</summary>
    <author>
      <name>Dongwon Han</name>
    </author>
    <author>
      <name>Neelima Sehgal</name>
    </author>
    <author>
      <name>Francisco Villaescusa-Navarro</name>
    </author>
    <arxiv:comment xmlns:arxiv="http://arxiv.org/schemas/atom">Sims are now public on NERSC at
  /global/cfs/cdirs/cmb/data/generic/mmDL; see also
  https://lambda.gsfc.nasa.gov/simulation/tb_sim_ov.cfm on LAMBDA</arxiv:comment>
    <link href="http://arxiv.org/abs/2105.11444v1" rel="alternate" type="text/html"/>
    <link title="pdf" href="http://arxiv.org/pdf/2105.11444v1" rel="related" type="application/pdf"/>
    <arxiv:primary_category xmlns:arxiv="http://arxiv.org/schemas/atom" term="astro-ph.CO" scheme="http://arxiv.org/schemas/atom"/>
    <category term="astro-ph.CO" scheme="http://arxiv.org/schemas/atom"/>
  </entry>
  <entry>
    <id>http://arxiv.org/abs/2104.14294v2</id>
    <updated>2021-05-24T17:49:18Z</updated>
    <published>2021-04-29T12:28:51Z</published>
    <title>Emerging Properties in Self-Supervised Vision Transformers</title>
    <summary>  In this paper, we question if self-supervised learning provides new
properties to Vision Transformer (ViT) that stand out compared to convolutional
networks (convnets). Beyond the fact that adapting self-supervised methods to
this architecture works particularly well, we make the following observations:
first, self-supervised ViT features contain explicit information about the
semantic segmentation of an image, which does not emerge as clearly with
supervised ViTs, nor with convnets. Second, these features are also excellent
k-NN classifiers, reaching 78.3% top-1 on ImageNet with a small ViT. Our study
also underlines the importance of momentum encoder, multi-crop training, and
the use of small patches with ViTs. We implement our findings into a simple
self-supervised method, called DINO, which we interpret as a form of
self-distillation with no labels. We show the synergy between DINO and ViTs by
achieving 80.1% top-1 on ImageNet in linear evaluation with ViT-Base.
</summary>
    <author>
      <name>Mathilde Caron</name>
    </author>
    <author>
      <name>Hugo Touvron</name>
    </author>
    <author>
      <name>Ishan Misra</name>
    </author>
    <author>
      <name>Hervé Jégou</name>
    </author>
    <author>
      <name>Julien Mairal</name>
    </author>
    <author>
      <name>Piotr Bojanowski</name>
    </author>
    <author>
      <name>Armand Joulin</name>
    </author>
    <arxiv:comment xmlns:arxiv="http://arxiv.org/schemas/atom">21 pages</arxiv:comment>
    <link href="http://arxiv.org/abs/2104.14294v2" rel="alternate" type="text/html"/>
    <link title="pdf" href="http://arxiv.org/pdf/2104.14294v2" rel="related" type="application/pdf"/>
    <arxiv:primary_category xmlns:arxiv="http://arxiv.org/schemas/atom" term="cs.CV" scheme="http://arxiv.org/schemas/atom"/>
    <category term="cs.CV" scheme="http://arxiv.org/schemas/atom"/>
  </entry>
  <entry>
    <id>http://arxiv.org/abs/2105.11427v1</id>
    <updated>2021-05-24T17:34:57Z</updated>
    <published>2021-05-24T17:34:57Z</published>
    <title>Attention-guided Temporal Coherent Video Object Matting</title>
    <summary>  This paper proposes a novel deep learning-based video object matting method
that can achieve temporally coherent matting results. Its key component is an
attention-based temporal aggregation module that maximizes image matting
networks' strength for video matting networks. This module computes temporal
correlations for pixels adjacent to each other along the time axis in feature
space to be robust against motion noises. We also design a novel loss term to
train the attention weights, which drastically boosts the video matting
performance. Besides, we show how to effectively solve the trimap generation
problem by fine-tuning a state-of-the-art video object segmentation network
with a sparse set of user-annotated keyframes. To facilitate video matting and
trimap generation networks' training, we construct a large-scale video matting
dataset with 80 training and 28 validation foreground video clips with
ground-truth alpha mattes. Experimental results show that our method can
generate high-quality alpha mattes for various videos featuring appearance
change, occlusion, and fast motion. Our code and dataset can be found at
https://github.com/yunkezhang/TCVOM
</summary>
    <author>
      <name>Yunke Zhang</name>
    </author>
    <author>
      <name>Chi Wang</name>
    </author>
    <author>
      <name>Miaomiao Cui</name>
    </author>
    <author>
      <name>Peiran Ren</name>
    </author>
    <author>
      <name>Xuansong Xie</name>
    </author>
    <author>
      <name>Xian-sheng Hua</name>
    </author>
    <author>
      <name>Hujun Bao</name>
    </author>
    <author>
      <name>Qixing Huang</name>
    </author>
    <author>
      <name>Weiwei Xu</name>
    </author>
    <arxiv:comment xmlns:arxiv="http://arxiv.org/schemas/atom">10 pages, 6 figures</arxiv:comment>
    <link href="http://arxiv.org/abs/2105.11427v1" rel="alternate" type="text/html"/>
    <link title="pdf" href="http://arxiv.org/pdf/2105.11427v1" rel="related" type="application/pdf"/>
    <arxiv:primary_category xmlns:arxiv="http://arxiv.org/schemas/atom" term="cs.CV" scheme="http://arxiv.org/schemas/atom"/>
    <category term="cs.CV" scheme="http://arxiv.org/schemas/atom"/>
  </entry>
  <entry>
    <id>http://arxiv.org/abs/2010.08852v2</id>
    <updated>2021-05-24T17:31:04Z</updated>
    <published>2020-10-17T19:28:35Z</published>
    <title>Weight-Covariance Alignment for Adversarially Robust Neural Networks</title>
    <summary>  Stochastic Neural Networks (SNNs) that inject noise into their hidden layers
have recently been shown to achieve strong robustness against adversarial
attacks. However, existing SNNs are usually heuristically motivated, and often
rely on adversarial training, which is computationally costly. We propose a new
SNN that achieves state-of-the-art performance without relying on adversarial
training, and enjoys solid theoretical justification. Specifically, while
existing SNNs inject learned or hand-tuned isotropic noise, our SNN learns an
anisotropic noise distribution to optimize a learning-theoretic bound on
adversarial robustness. We evaluate our method on a number of popular
benchmarks, show that it can be applied to different architectures, and that it
provides robustness to a variety of white-box and black-box attacks, while
being simple and fast to train compared to existing alternatives.
</summary>
    <author>
      <name>Panagiotis Eustratiadis</name>
    </author>
    <author>
      <name>Henry Gouk</name>
    </author>
    <author>
      <name>Da Li</name>
    </author>
    <author>
      <name>Timothy Hospedales</name>
    </author>
    <link href="http://arxiv.org/abs/2010.08852v2" rel="alternate" type="text/html"/>
    <link title="pdf" href="http://arxiv.org/pdf/2010.08852v2" rel="related" type="application/pdf"/>
    <arxiv:primary_category xmlns:arxiv="http://arxiv.org/schemas/atom" term="cs.LG" scheme="http://arxiv.org/schemas/atom"/>
    <category term="cs.LG" scheme="http://arxiv.org/schemas/atom"/>
    <category term="cs.CR" scheme="http://arxiv.org/schemas/atom"/>
    <category term="cs.CV" scheme="http://arxiv.org/schemas/atom"/>
  </entry>
</feed>
